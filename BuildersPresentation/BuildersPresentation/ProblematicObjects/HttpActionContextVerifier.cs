﻿using System.Net.Http;
using System.Web.Http.Controllers;

namespace BuildersPresentation.ProblematicObjects
{
    public class HttpActionContextVerifier
    {
        public RequestVerificationResult Verify(HttpActionContext context)
        {
            if (context.Request.Method == HttpMethod.Post)
            {
                return RequestVerificationResult.Invalid;
            }

            if (context.Request.RequestUri.ToString().Contains("dupa"))
            {
                return RequestVerificationResult.Vulgar;
            }

            return RequestVerificationResult.Correct;
        }
    }
}