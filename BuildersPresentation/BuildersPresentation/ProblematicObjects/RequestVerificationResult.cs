namespace BuildersPresentation.ProblematicObjects
{
    public enum RequestVerificationResult
    {
        Correct,
        Vulgar,
        Invalid
    }
}