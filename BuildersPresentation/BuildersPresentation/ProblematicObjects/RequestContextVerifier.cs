﻿using System.Web.Routing;

namespace BuildersPresentation.ProblematicObjects
{
    public class RequestContextVerifier
    {
        public RequestVerificationResult Verify(RequestContext context)
        {
            if (context.HttpContext.Request.HttpMethod == "POST")
            {
                return RequestVerificationResult.Invalid;
            }

            if (context.HttpContext.Request.RawUrl.Contains("dupa"))
            {
                return RequestVerificationResult.Vulgar;
            }

            return RequestVerificationResult.Correct;
        }
    }
}