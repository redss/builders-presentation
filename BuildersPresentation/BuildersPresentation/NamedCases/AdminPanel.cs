﻿using System;

namespace BuildersPresentation.NamedCases
{
    public class AdminPanel
    {
        public void BanUser(User currentUser, User userToBan, string banReason)
        {
            if (currentUser.Role != Role.Admin)
            {
                var message = string.Format("User {0} is not administrator and cannot ban!", currentUser.Name);

                throw new InvalidOperationException(message);
            }

            if (userToBan.Role == Role.Admin)
            {
                var message = string.Format("User {0} is an admin and cannot be banned!", userToBan.Name);

                throw new InvalidOperationException(message);
            }

            if (userToBan.BannedDate != null)
            {
                var message = string.Format("User {0} is already banned!", userToBan.Name);

                throw new InvalidOperationException(message);
            }

            userToBan.BannedDate = DateTime.Now; // For tests purposes only!
            userToBan.BanReason = banReason;
        }
    }
}