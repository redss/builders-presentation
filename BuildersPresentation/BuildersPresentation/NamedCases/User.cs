﻿using System;

namespace BuildersPresentation.NamedCases
{
    public enum Role
    {
        Admin, Moderator, User
    }

    public class User
    {
        public string Name { get; set; }
        public Role Role { get; set; }
        public DateTime? BannedDate { get; set; }
        public string BanReason { get; set; }
    }
}