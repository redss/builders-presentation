﻿using System.Collections.Generic;
using System.Linq;

namespace BuildersPresentation.ChangingObjects
{
    public class ValidationResult
    {
        private readonly string[] _invalidProperties;

        public ValidationResult(IEnumerable<string> invalidProperties)
        {
            _invalidProperties = invalidProperties.ToArray();
        }

        public IEnumerable<string> InvalidProperties
        {
            get { return _invalidProperties; }
        }

        public bool IsInvalid
        {
            get { return _invalidProperties.Any(); }
        }

        public bool IsValid
        {
            get { return !IsInvalid; }
        }
    }
}