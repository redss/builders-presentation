﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BuildersPresentation.ChangingObjects
{
    public class PersonValidator
    {
        private readonly EmailAddressAttribute _emailAddressAttribute = new EmailAddressAttribute(); 

        public ValidationResult Validate(Person person)
        {
            var invalidProperties = GetValidationErrors(person);

            return new ValidationResult(invalidProperties);
        }

        private IEnumerable<string> GetValidationErrors(Person person)
        {
            if (string.IsNullOrEmpty(person.Name))
            {
                yield return "Name";
            }

            if (string.IsNullOrEmpty(person.Surname))
            {
                yield return "Surname";
            }

            if (person.Age < 0)
            {
                yield return "Age";
            }

            if (!_emailAddressAttribute.IsValid(person.Email))
            {
                yield return "Email";
            }
        }
    }
}