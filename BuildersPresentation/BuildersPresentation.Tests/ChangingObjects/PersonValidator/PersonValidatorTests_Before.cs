﻿using BuildersPresentation.ChangingObjects;
using NUnit.Framework;

// ReSharper disable InconsistentNaming

namespace BuildersPresentation.Tests.ChangingObjects.PersonValidator
{
    public class PersonValidatorTests_Before
    {
        public BuildersPresentation.ChangingObjects.PersonValidator Sut;

        [SetUp]
        public void SetUp()
        {
            Sut = new BuildersPresentation.ChangingObjects.PersonValidator();
        }

        [Test]
        public void Can_Validate_Person()
        {
            // Arrange

            var person = new Person
            {
                Name = "Janek",
                Surname = "Jankowy",
                Age = 20,
                Email = "janek@mail.com"
            };

            // Act

            var result = Sut.Validate(person);

            // Assert

            Assert.That(result.IsValid);
            Assert.That(result.InvalidProperties, Is.Empty);
        }

        [TestCase(null)]
        [TestCase("")]
        public void Is_Invalid_When_Name_Is_Not_Provided(string name)
        {
            // Arrange

            var person = new Person
            {
                Name = name,
                Surname = "Jankowy",
                Age = 20,
                Email = "janek@mail.com"
            };

            // Act

            var result = Sut.Validate(person);

            // Assert

            Assert.That(result.IsInvalid);
            Assert.That(result.InvalidProperties, Is.EquivalentTo(new[] { "Name" }));
        }

        [TestCase(null)]
        [TestCase("")]
        public void Is_Invalid_When_Surname_Is_Not_Provided(string surname)
        {
            // Arrange

            var person = new Person
            {
                Name = "Janek",
                Surname = surname,
                Age = 20,
                Email = "janek@mail.com"
            };

            // Act

            var result = Sut.Validate(person);

            // Assert

            Assert.That(result.IsInvalid);
            Assert.That(result.InvalidProperties, Is.EquivalentTo(new[] { "Surname" }));
        }

        [TestCase(-1)]
        [TestCase(-20)]
        public void Is_Invalid_When_Age_Is_Negative(int age)
        {
            // Arrange

            var person = new Person
            {
                Name = "Janek",
                Surname = "Jankowy",
                Age = age,
                Email = "janek@mail.com"
            };

            // Act

            var result = Sut.Validate(person);

            // Assert

            Assert.That(result.IsInvalid);
            Assert.That(result.InvalidProperties, Is.EquivalentTo(new[] { "Age" }));
        }

        [Test]
        public void Is_Invalid_When_Email_Is_Incorrect()
        {
            // Arrange

            var person = new Person
            {
                Name = "Janek",
                Surname = "Jankowy",
                Age = 20,
                Email = "oops!"
            };

            // Act

            var result = Sut.Validate(person);

            // Assert

            Assert.That(result.IsInvalid);
            Assert.That(result.InvalidProperties, Is.EquivalentTo(new[] { "Email" }));
        }
    }
}