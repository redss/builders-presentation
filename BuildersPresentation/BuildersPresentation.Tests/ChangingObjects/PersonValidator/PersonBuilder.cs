﻿using BuildersPresentation.ChangingObjects;

namespace BuildersPresentation.Tests.ChangingObjects.PersonValidator
{
    public class PersonBuilder
    {
        private readonly Person _person = new Person
        {
            Name = "Janek",
            Surname = "Jankowy",
            Age = 20,
            Email = "janek@mail.com"
        };
        
        public PersonBuilder WithName(string name)
        {
            _person.Name = name;
            return this;
        }

        public PersonBuilder WithSurname(string surname)
        {
            _person.Surname = surname;
            return this;
        }
        
        public PersonBuilder WithAge(int age)
        {
            _person.Age = age;
            return this;
        }

        public PersonBuilder WithEmail(string email)
        {
            _person.Email = email;
            return this;
        }

        public Person Build()
        {
            return _person;
        }
    }
}