﻿using System.Net.Http;
using System.Web.Http.Controllers;

namespace BuildersPresentation.Tests.ProblematicObjects.HttpActionContextVerifier
{
    public class HttpActionContextBuilder
    {
        private HttpMethod _httpMethod = HttpMethod.Get;
        private string _url = "http://localhost/";

        public HttpActionContextBuilder WithHttpMethod(HttpMethod httpMethod)
        {
            _httpMethod = httpMethod;
            return this;
        }

        public HttpActionContextBuilder WithUrl(string url)
        {
            _url = url;
            return this;
        }

        public HttpActionContext Build()
        {
            return new HttpActionContext
            {
                ControllerContext = new HttpControllerContext
                {
                    Request = new HttpRequestMessage(_httpMethod, _url)
                }
            };
        }
    }
}