using System.Net.Http;
using BuildersPresentation.ProblematicObjects;
using NUnit.Framework;

// ReSharper disable InconsistentNaming

namespace BuildersPresentation.Tests.ProblematicObjects.HttpActionContextVerifier
{
    public class HttpActionContextVerifierTests_After
    {
        public BuildersPresentation.ProblematicObjects.HttpActionContextVerifier Sut;

        [SetUp]
        public void SetUp()
        {
            Sut = new BuildersPresentation.ProblematicObjects.HttpActionContextVerifier();
        }

        [Test]
        public void Typical_Request_Is_Correct()
        {
            // Arrange

            var context = new HttpActionContextBuilder()
                .Build();

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Correct));
        }

        [Test]
        public void Post_Request_Is_Invalid()
        {
            // Arrange

            var context = new HttpActionContextBuilder()
                .WithHttpMethod(HttpMethod.Post)
                .Build();

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Invalid));
        }

        [Test]
        public void Request_With_Swearing_In_Url_Is_Vulgar()
        {
            // Arrange

            var context = new HttpActionContextBuilder()
                .WithUrl("http://localhost/dupa")
                .Build();

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Vulgar));
        }
    }
}