﻿using System.Net.Http;
using System.Web.Http.Controllers;
using BuildersPresentation.ProblematicObjects;
using NUnit.Framework;

// ReSharper disable InconsistentNaming

namespace BuildersPresentation.Tests.ProblematicObjects.HttpActionContextVerifier
{
    public class HttpActionContextVerifierTests_Before
    {
        public BuildersPresentation.ProblematicObjects.HttpActionContextVerifier Sut;

        [SetUp]
        public void SetUp()
        {
            Sut = new BuildersPresentation.ProblematicObjects.HttpActionContextVerifier();
        }

        [Test]
        public void Typical_Request_Is_Correct()
        {
            // Arrange

            var context = new HttpActionContext
            {
                ControllerContext = new HttpControllerContext
                {
                    Request = new HttpRequestMessage(HttpMethod.Get, "http://localhost/foo")
                }
            };

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Correct));
        }

        [Test]
        public void Post_Request_Is_Invalid()
        {
            // Arrange

            var context = new HttpActionContext
            {
                ControllerContext = new HttpControllerContext
                {
                    Request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/foo")
                }
            };

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Invalid));
        }

        [Test]
        public void Request_With_Swearing_In_Url_Is_Vulgar()
        {
            // Arrange

            var context = new HttpActionContext
            {
                ControllerContext = new HttpControllerContext
                {
                    Request = new HttpRequestMessage(HttpMethod.Get, "http://localhost/dupa")
                }
            };

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Vulgar));
        }
    }
}