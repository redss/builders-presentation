﻿using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Web.Routing;
using FakeItEasy;

// ReSharper disable RedundantArgumentNameForLiteralExpression
// ReSharper disable RedundantArgumentName

namespace BuildersPresentation.Tests.ProblematicObjects.RequestContextVerifier
{
    public class RequestContextBuilder
    {
        private string _httpMethod = "GET";
        private string _virtualPath = "/foo";

        public RequestContextBuilder WithHttpMethod(string httpMethod)
        {
            _httpMethod = httpMethod;
            return this;
        }

        public RequestContextBuilder WithVirtualPath(string virtualPath)
        {
            _virtualPath = virtualPath;
            return this;
        }

        public RequestContext Build()
        {
            var simpleWorkerRequest = new SimpleWorkerRequest(
                appVirtualDir: _virtualPath,
                appPhysicalDir: "",
                page: "",
                query: "",
                output: new StringWriter());

            var httpWorkerRequest = A.Fake<SimpleWorkerRequest>(builder =>
                builder.Wrapping(simpleWorkerRequest));

            A.CallTo(() => httpWorkerRequest.GetHttpVerbName())
                .Returns(_httpMethod);

            A.CallTo(() => httpWorkerRequest.GetUriPath())
                .Returns(_virtualPath);

            var httpContext = new HttpContext(httpWorkerRequest);

            var httpContextWrapper = new HttpContextWrapper(httpContext);

            return new RequestContext
            {
                HttpContext = httpContextWrapper
            };
        }
    }
}