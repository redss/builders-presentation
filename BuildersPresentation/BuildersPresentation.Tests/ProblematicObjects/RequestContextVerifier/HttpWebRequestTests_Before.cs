﻿using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Web.Routing;
using BuildersPresentation.ProblematicObjects;
using FakeItEasy;
using NUnit.Framework;

// ReSharper disable InconsistentNaming
// ReSharper disable RedundantArgumentNameForLiteralExpression
// ReSharper disable RedundantArgumentName

namespace BuildersPresentation.Tests.ProblematicObjects.RequestContextVerifier
{
    public class HttpWebRequestTests_Before
    {
        public BuildersPresentation.ProblematicObjects.RequestContextVerifier Sut;

        [SetUp]
        public void SetUp()
        {
            Sut = new BuildersPresentation.ProblematicObjects.RequestContextVerifier();
        }

        [Test]
        public void Typical_Request_Is_Correct()
        {
            // Arrange

            var simpleWorkerRequest = new SimpleWorkerRequest(
                appVirtualDir: "",
                appPhysicalDir: "",
                page: "",
                query: "",
                output: new StringWriter());

            var httpWorkerRequest = A.Fake<SimpleWorkerRequest>(builder =>
                builder.Wrapping(simpleWorkerRequest));

            A.CallTo(() => httpWorkerRequest.GetHttpVerbName())
                .Returns("GET");

            var httpContext = new HttpContext(httpWorkerRequest);

            var context = new RequestContext
            {
                HttpContext = new HttpContextWrapper(httpContext)
            };

            // Act

            var result = Sut.Verify(context);

            // Assert
            
            Assert.That(result, Is.EqualTo(RequestVerificationResult.Correct));
        }

        [Test]
        public void Post_Request_Is_Invalid()
        {
            // Arrange

            var simpleWorkerRequest = new SimpleWorkerRequest(
                appVirtualDir: "",
                appPhysicalDir: "",
                page: "",
                query: "",
                output: new StringWriter());

            var httpWorkerRequest = A.Fake<SimpleWorkerRequest>(builder =>
                builder.Wrapping(simpleWorkerRequest));

            A.CallTo(() => httpWorkerRequest.GetHttpVerbName())
                .Returns("POST");

            var httpContext = new HttpContext(httpWorkerRequest);

            var context = new RequestContext
            {
                HttpContext = new HttpContextWrapper(httpContext)
            };

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Invalid));
        }

        [Test]
        public void Request_With_Swearing_In_Url_Is_Vulgar()
        {
            // Arrange

            var simpleWorkerRequest = new SimpleWorkerRequest(
                appVirtualDir: "/dupa",
                appPhysicalDir: @"C:\aaa",
                page: "",
                query: "",
                output: new StringWriter());

            var httpWorkerRequest = A.Fake<SimpleWorkerRequest>(builder =>
                builder.Wrapping(simpleWorkerRequest));

            A.CallTo(() => httpWorkerRequest.GetHttpVerbName())
                .Returns("GET");

            var httpContext = new HttpContext(httpWorkerRequest);

            var context = new RequestContext
            {
                HttpContext = new HttpContextWrapper(httpContext)
            };

            // Act

            var result = Sut.Verify(context);

            // Assert
            
            Assert.That(result, Is.EqualTo(RequestVerificationResult.Vulgar));
        }
    }
}