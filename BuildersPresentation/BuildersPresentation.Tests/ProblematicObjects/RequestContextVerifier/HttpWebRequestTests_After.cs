using BuildersPresentation.ProblematicObjects;
using NUnit.Framework;

// ReSharper disable InconsistentNaming
// ReSharper disable RedundantArgumentNameForLiteralExpression
// ReSharper disable RedundantArgumentName

namespace BuildersPresentation.Tests.ProblematicObjects.RequestContextVerifier
{
    public class HttpWebRequestTests_After
    {
        public BuildersPresentation.ProblematicObjects.RequestContextVerifier Sut;

        [SetUp]
        public void SetUp()
        {
            Sut = new BuildersPresentation.ProblematicObjects.RequestContextVerifier();
        }

        [Test]
        public void Typical_Request_Is_Correct()
        {
            // Arrange

            var context = new RequestContextBuilder()
                .Build();

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Correct));
        }

        [Test]
        public void Post_Request_Is_Invalid()
        {
            // Arrange

            var context = new RequestContextBuilder()
                .WithHttpMethod("POST")
                .Build();

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Invalid));
        }

        [Test]
        public void Request_With_Swearing_In_Url_Is_Vulgar()
        {
            // Arrange

            var context = new RequestContextBuilder()
                .WithVirtualPath("/dupa")
                .Build();

            // Act

            var result = Sut.Verify(context);

            // Assert

            Assert.That(result, Is.EqualTo(RequestVerificationResult.Vulgar));
        }
    }
}