﻿using System;
using BuildersPresentation.NamedCases;

// ReSharper disable InconsistentNaming

namespace BuildersPresentation.Tests.NamedCases
{
    public class UserBuilder
    {
        private readonly User _user = new User
        {
            Name = "Guy",
            Role = Role.User,
            BannedDate = null,
            BanReason = null
        };

        public UserBuilder WithName(string name)
        {
            _user.Name = name;
            return this;
        }

        public UserBuilder WithRole(Role role)
        {
            _user.Role = role;
            return this;
        }

        public UserBuilder WithBannedDate(DateTime? dateTime)
        {
            _user.BannedDate = dateTime;
            return this;
        }

        public UserBuilder WithBanReason(string banReason)
        {
            _user.BanReason = banReason;
            return this;
        }

        public UserBuilder WhichIsAdmin()
        {
            _user.Role = Role.Admin;
            return this;
        }

        public UserBuilder WhichIsNormalUser()
        {
            _user.Role = Role.User;
            return this;
        }

        public UserBuilder WhichIsBanned()
        {
            _user.BannedDate = new DateTime(2014, 6, 2);
            _user.BanReason = "For money";
            return this;
        }

        public UserBuilder WhichIsNotBanned()
        {
            _user.BannedDate = null;
            _user.BanReason = null;
            return this;
        }

        public User Build()
        {
            return _user;
        }
    }
}