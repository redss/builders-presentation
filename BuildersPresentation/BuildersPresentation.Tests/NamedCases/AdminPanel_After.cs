﻿using System;
using BuildersPresentation.NamedCases;
using NUnit.Framework;

// ReSharper disable InconsistentNaming

namespace BuildersPresentation.Tests.NamedCases
{
    public class AdminPanel_After
    {
        public AdminPanel Sut;

        [SetUp]
        public void SetUp()
        {
            Sut = new AdminPanel();
        }

        [Test]
        public void Can_Ban_User()
        {
            // Arrange

            var currentUser = new UserBuilder()
                .WhichIsAdmin()
                .Build();

            var userToBan = new UserBuilder()
                .WhichIsNotBanned()
                .Build();

            // Act

            Sut.BanUser(currentUser, userToBan, "Swearing");

            // Assert

            Assert.That(userToBan.BannedDate, Is.Not.Null);
            Assert.That(userToBan.BanReason, Is.EqualTo("Swearing"));
        }

        [Test]
        public void Throws_Exception_When_Current_User_Is_Not_An_Admin()
        {
            // Arrange

            var currentUser = new UserBuilder()
                .WhichIsNormalUser()
                .WithName("Roger")
                .Build();

            var userToBan = new UserBuilder()
                .WhichIsNotBanned()
                .Build();

            // Act, Assert

            var exception = Assert.Throws<InvalidOperationException>(() =>
                Sut.BanUser(currentUser, userToBan, "Swearing"));

            Assert.That(exception.Message, Is.EqualTo("User Roger is not administrator and cannot ban!"));
        }

        [Test]
        public void Throws_Exception_When_User_Was_Banned()
        {
            // Arrange

            var currentUser = new UserBuilder()
                .WhichIsAdmin()
                .Build();

            var userToBan = new UserBuilder()
                .WithName("Jack")
                .WhichIsBanned()
                .Build();

            // Act, Assert

            var exception = Assert.Throws<InvalidOperationException>(() =>
                Sut.BanUser(currentUser, userToBan, "Swearing"));

            Assert.That(exception.Message, Is.EqualTo("User Jack is already banned!"));
        }

        [Test]
        public void Throws_Exception_When_User_Is_An_Admin()
        {
            // Arrange

            var currentUser = new UserBuilder()
                .WhichIsAdmin()
                .Build();

            var userToBan = new UserBuilder()
                .WhichIsAdmin()
                .WhichIsNotBanned()
                .WithName("Jack")
                .Build();

            // Act, Assert

            var exception = Assert.Throws<InvalidOperationException>(() =>
                Sut.BanUser(currentUser, userToBan, "Swearing"));

            Assert.That(exception.Message, Is.EqualTo("User Jack is an admin and cannot be banned!"));
        }
    }
}