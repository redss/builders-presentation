﻿using BuildersPresentation.NamedCases;

namespace BuildersPresentation.Tests.NamedCases
{
    public class UserToBanBuilder
    {
        private readonly UserBuilder _userBuilder = new UserBuilder()
            .WhichIsNotBanned();

        public UserToBanBuilder WithName(string name)
        {
            _userBuilder.WithName(name);
            return this;
        }

        public UserToBanBuilder WhichIsAdmin()
        {
            _userBuilder.WhichIsAdmin();
            return this;
        }

        public UserToBanBuilder WhichWasAlreadyBanned()
        {
            _userBuilder.WhichIsBanned();
            return this;
        }

        public User Build()
        {
            return _userBuilder.Build();
        }
    }
}