﻿using BuildersPresentation.NamedCases;

namespace BuildersPresentation.Tests.NamedCases
{
    public class BanningUserBuilder
    {
        private readonly UserBuilder _userBuilder = new UserBuilder()
            .WhichIsAdmin();

        public BanningUserBuilder WhichIsNotAdmin()
        {
            _userBuilder.WhichIsNormalUser();
            return this;
        }

        public BanningUserBuilder WithName(string name)
        {
            _userBuilder.WithName(name);
            return this;
        }

        public User Build()
        {
            return _userBuilder.Build();
        }
    }
}